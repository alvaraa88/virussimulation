# Virus Simulation

Hello, Thanks for checking out my Virus Simulation.
Here are some of the main points I would like to express as the objective of this project.
1) Java fundamentals
2) JavaFX fundamentals(GUI fundamentals)
3) OOP(Object Oriented Programming)

## Getting started:
This project is a virus simulator. it is supposed to show how people get sick from 1 virus and pass it on with contact.
### how it works:
* This project has sliders to change:
  * the speed of movement, the size of targets, distance a person can move and a recovery time.
  * It has two graphs to show the correlation between nonsick, sick , and recovered people.
  * it has a Begin, Start , Stop and Step button.
    * Begin must be pushed first to start the simulation.


## About Project
* This program consists of 2 packages. app and model the model package contains a model of 
* movingPosition, a Person, Position, Simulation, and an ENUM class named State.
>The **VirusController** is responsible for combining all the model classes and 
> forming a working simulation via a GUI.

>The **HeadPosition** is responsible for the head position of the person 
> and the speed at which a person will go.

>The **Position** is responsible for the modeling current location, distance of another person and movement.

>The **Person** is responsible for the modeling of a person. 

>The **Sim** is responsible for drawing of pane and it's components. 

>The **State** is responsible for the state of a person.


### Author and website:
1) [Alexander Alvara](https://alexanderalvara.com/resume)
2) [Gitlab](https://gitlab.com/alvaraa88/virussimulation)
3) [LinkedIn](https://www.linkedin.com/in/alexander-alvara-4132a3a7/)
