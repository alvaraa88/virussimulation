package app;

import javafx.animation.AnimationTimer;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import model.MovingPosition;
import model.Person;
import model.Sim;
import model.State;
import java.net.URL;
import java.util.EnumMap;
import java.util.ResourceBundle;

/**
 * this class is the Controller part of the program.
 * it glues all the pieces together.
 */
public class VirusController implements Initializable {
    @FXML
    Pane paneVirusSimulation;
    @FXML
    Pane paneChart;
    @FXML
    Pane paneHistogram;
    @FXML
    Button buttonStart;
    @FXML
    Button buttonStep;
    @FXML
    Button buttonStop;
    @FXML
    Button buttonBegin;
    @FXML
    Slider sliderSize;
    @FXML
    Slider sliderRecovery;
    @FXML
    Slider sliderDistance;
    @FXML
    Slider sliderSpeed;
    @FXML
    TextField textFieldTicks;
    EnumMap<State, Rectangle> histogramRectangles = new EnumMap<>(State.class);
    Sim sim;

    private Movement clock;
    private class Movement extends AnimationTimer{
        private long FRAMES_PER_SECOND = 50L;
        private long INTERVAL = 1000000000L / FRAMES_PER_SECOND;
        private long last = 0;
        private int ticks = 0;

        @Override
        public void handle(long now){
            if(now - last > INTERVAL){
                step();
                last = now;
                tick();
            }
        }
        public int getTicks(){
            return ticks;
        }
        public void resetTicks(){
            ticks = 0;
        }

        public void tick(){
            ticks++;
        }
    }

    @FXML
    public void begin(){
        stop();
        clock.resetTicks();
        textFieldTicks.setText("" + clock.getTicks());
        paneVirusSimulation.getChildren().clear();
        paneChart.getChildren().clear();
        paneHistogram.getChildren().clear();
        sim = new Sim(paneVirusSimulation,100);
        setDistance();
        setSize();
        setRecovery();
        int offset = 0;
        for(State state : State.values()){
            Rectangle rect = new Rectangle(60, 0, state.getColor());
            rect.setTranslateX(offset);
            offset += 65;
            histogramRectangles.put(state, rect);
            paneHistogram.getChildren().add(rect);
        }
        drawCharts();
    }

    @FXML
    public void step(){
        sim.move();
        sim.recover();
        sim.resolveCollisions();
        sim.draw();
        clock.tick();
        textFieldTicks.setText("" + clock.getTicks());
        drawCharts();
    }
    @FXML
    public void start(){
        clock.start();
        disableButtons(true, false, true);
    }

    @FXML
    public void stop(){
        clock.stop();
        disableButtons(false, true, false);
    }

    public void disableButtons(boolean start, boolean stop, boolean step){
        buttonStart.setDisable(start);
        buttonStop.setDisable(stop);
        buttonStep.setDisable(step);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        sliderSize.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setSize();
            }
        });
        sliderRecovery.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setRecovery();
            }
        });
        sliderDistance.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setDistance();
            }
        });
        sliderSpeed.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setSpeed();
            }
        });
        clock = new Movement();
        paneVirusSimulation.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
    }

    public void setSize(){
        Person.radius = (int) sliderSize.getValue();
        sim.draw();
    }

    public void setDistance(){
        Person.distance = (int) sliderDistance.getValue();
    }
    //abstract 50 from method
    public void setRecovery(){
        Person.recoverTime = 50 * (int)sliderRecovery.getValue();
        sim.draw();
    }
    public void setSpeed(){
        MovingPosition.SPEED = (int) sliderSpeed.getValue();
    }
    public void drawCharts(){
        EnumMap<State, Integer> currentPop = new EnumMap<State, Integer>(State.class);
        for(Person person: sim.getPeople()){
            if(!currentPop.containsKey(person.getState())){
                currentPop.put(person.getState(),0);
            }
            currentPop.put(person.getState(), 1 + currentPop.get(person.getState()));
        }
        for(State state : histogramRectangles.keySet()){
            if(currentPop.containsKey(state)){
                histogramRectangles.get(state).setHeight(currentPop.get(state));
                //translate rects from bottom to top.
                histogramRectangles.get(state).setTranslateY(30 + 100 - currentPop.get(state));

                Circle circle = new Circle(1, state.getColor());
                circle.setTranslateX(clock.getTicks() / 5.0);
                circle.setTranslateY(130 - currentPop.get(state));
                paneChart.getChildren().add(circle);
            }
        }
        if(!currentPop.containsKey(State.SICK)){
            stop();
        }
    }
}
