package model;

/**
 * this class is the model for a position.
 */
public class MovingPosition {
    private double dx;
    private double dy;
    public static int SPEED = 3;

    //will be in random state
    public MovingPosition(){
        double direction = Math.random() * 2 * Math.PI;
        dx = Math.sin(direction);
        dy = Math.cos(direction);
    }
    public double getDx(){
        return dx * SPEED;
    }

    public double getDy(){
        return dy * SPEED;
    }

    public void bounceX(){
        dx *= -1;
    }
    public void bounceY(){
        dy *= -1;
    }

}
