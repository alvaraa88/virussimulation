package model;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * This class represents the state of each person on the pane.
 */
public class Person {
    private Pane pane;
    private State state;
    private Position position;
    private Position origin;
    private MovingPosition heading;
    private Circle circle;
    private int sickTime = 0;
    public static int radius = 5;
    public static int recoverTime = 5 * 50; // 50 ticks per second
    public static int distance = 200;

    public Person(State state, Pane pane){
        this.state = state;
        this.heading = new MovingPosition();
        this.position = new Position(pane);
        //this is a deep copy of the original position, since position will be changing.
        this.origin = new Position(position.getX(), position.getY());
        this.circle = new Circle(radius, state.getColor());
        this.pane = pane;
        circle.setStroke(Color.BLACK);
        pane.getChildren().add(circle);
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
        circle.setFill(state.getColor());
    }

    public void move(){
        position.move(heading, pane, origin);
    }

    public void draw(){
        circle.setRadius(radius);
        circle.setTranslateX(position.getX());
        circle.setTranslateY(position.getY());
    }
    public void recover(){
        if(state == State.SICK){
            sickTime++;
            if(sickTime > recoverTime){
                setState(State.RECOVERED);
            }
        }
    }

    public void collisionCheck(Person other){
        if(position.collision(other.position)){
            if(other.getState() == State.SICK && state == State.NOT_SICK){
                setState(State.SICK);
            }
        }
    }

}
