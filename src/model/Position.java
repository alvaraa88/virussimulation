package model;

import javafx.scene.layout.Pane;

/**
 * This class represents the position of each person at its current location.
 */
public class Position {
    private double x;
    private double y;

    public Position(double x, double y) {
        this.x = x;
        this.y = y;
    }

    //Math why?
    //random position on pane. ==> math.random
    //within the bounds of the circle ==> -2 *radius --> 1 for each side of the width and height
    // radius + Math.random ==> generate numbers from radius to 2*radius
    public Position(Pane world) {
        this(Person.radius + Math.random() * (world.getWidth() - 2 * Person.radius),
                Person.radius + Math.random() * (world.getHeight() - 2 * Person.radius));
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    //Euclidean distance
    public double distance(Position other) {
        return Math.sqrt(Math.pow(this.x - other.x, 2) + Math.pow(this.y - other.y, 2));
    }

    //distance between other positions
    public boolean collision(Position other) {
        return distance(other) < 2 * Person.radius;
    }

    public void move(MovingPosition movingPosition, Pane pane, Position origin) {
        x += movingPosition.getDx();
        y += movingPosition.getDy();
        if (x < Person.radius || x > pane.getWidth() - Person.radius || distance(origin) > Person.distance) {
            movingPosition.bounceX();
            x += movingPosition.getDx();
        }
        if (y < Person.radius || y > pane.getHeight() - Person.radius || distance(origin) > Person.distance) {
            movingPosition.bounceY();
            y += movingPosition.getDy();
        }
    }

}
