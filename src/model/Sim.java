package model;

import javafx.scene.layout.Pane;

import java.util.ArrayList;

/**
 * This class instantiates the population and drawing of panes
 */
public class Sim {

    private ArrayList<Person> people;

    public Sim(Pane pane, int populationSize) {
        people = new ArrayList<>();
        for (int i = 0; i < populationSize; i++) {
            people.add(new Person(State.NOT_SICK, pane));
        }
        people.add(new Person(State.SICK, pane));
        draw();
    }

    public ArrayList<Person> getPeople() {
        return people;
    }

    public void move() {
        for (Person person : people) {
            person.move();
        }
    }

    public void draw() {
        for (Person person : people) {
            person.draw();
        }
    }

    public void resolveCollisions() {
        for (Person person : people) {
            for (Person otherPerson : people) {
                if (person != otherPerson) {
                    person.collisionCheck(otherPerson);
                }
            }
        }
    }

    public void recover() {
        for (Person person : people) {
            person.recover();
        }
    }

}
