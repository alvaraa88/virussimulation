package model;

import javafx.scene.paint.Color;

/*
 * this class is used for the state of each person.
 */
public enum State {
    NOT_SICK {
        public Color getColor() {
            return Color.LIGHTGOLDENRODYELLOW;
        }
    }, SICK {
        public Color getColor() {
            return Color.RED;
        }
    }, RECOVERED {
        public Color getColor() {
            return Color.LIGHTBLUE;
        }
    };

    public abstract Color getColor();
}
